package ex1.company.Main;

import java.util.ArrayList;

public class Run {

    public static void main(String[] args) {
        Bear bear = new Bear("Po");
        Zebra zebra = new Zebra("Marti");
        Monkey monkey = new Monkey("Rafiki");
        Shark shark = new Shark("Bugor");

        ZooBox<Bear> bearZooBox = new ZooBox<>();
        ZooBox<Zebra> zebraZooBox = new ZooBox<>();
        ZooBox<Monkey> monkeyZooBox = new ZooBox<>();
        ZooBox<Shark> sharkZooBox = new ZooBox<>();

        bearZooBox.lockAnimal(bear);
        zebraZooBox.lockAnimal(zebra);
        monkeyZooBox.lockAnimal(monkey);
        sharkZooBox.lockAnimal(shark);

        bearZooBox.getAnimal().sayName();
        zebraZooBox.getAnimal().sayName();
        monkeyZooBox.getAnimal().sayName();
        sharkZooBox.getAnimal().sayName();

        ArrayList<ZooBox> zoo = new ArrayList<>();
        zoo.add(bearZooBox);
        zoo.add(zebraZooBox);
        zoo.add(monkeyZooBox);
        zoo.add(sharkZooBox);

        for (ZooBox z : zoo) z.getAnimal().sayName();
    }
}
