package ex1.company.Main;

import ex1.company.enums.TypeOfAnimal;

public abstract class Animals {

    protected String name;
    protected TypeOfAnimal typeOfAnimal;

    abstract void sayName();
}
