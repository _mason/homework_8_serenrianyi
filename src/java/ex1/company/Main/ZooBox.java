package ex1.company.Main;

public class ZooBox<T extends Animals> {

    private T animals;

    public T getAnimal() {
        return animals;
    }

    public void lockAnimal(T animal) {
        this.animals = animal;
    }
}
