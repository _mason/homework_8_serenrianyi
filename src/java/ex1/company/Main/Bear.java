package ex1.company.Main;

public class Bear extends Animals {

    public Bear(String name) {
        this.name=name;
        this.typeOfAnimal=typeOfAnimal.MAMMAL;
    }

    @Override
    void sayName() {
        System.out.println("I'm a <" + typeOfAnimal + ">. My name is <" + name + ">.");
    }
}
