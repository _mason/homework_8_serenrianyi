package ex1.company.Main;

public class Shark extends Animals {

    public Shark(String name) {
        this.name=name;
        this.typeOfAnimal=typeOfAnimal.FISH;
    }

    @Override
    void sayName() {
        System.out.println("I'm a <" + typeOfAnimal + ">. My name is <" + name + ">.");
    }
}
