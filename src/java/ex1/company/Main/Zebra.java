package ex1.company.Main;

public class Zebra extends Animals {

    public Zebra(String name) {
        this.name=name;
        this.typeOfAnimal=typeOfAnimal.MAMMAL;
    }

    @Override
    void sayName() {
        System.out.println("I'm a <" + typeOfAnimal + ">. My name is <" + name + ">.");
    }
}
