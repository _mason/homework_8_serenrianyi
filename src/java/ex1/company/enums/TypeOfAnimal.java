package ex1.company.enums;

public enum TypeOfAnimal {

    MAMMAL,     //(млекопитающее)
    REPTILIA,  //(земноводное)
    BIRD,       //(птица)
    FISH        //(рыба)
}
