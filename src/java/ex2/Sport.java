package ex2;

import java.util.List;

public class Sport<K extends Discipline, V extends Constituent> {

    private K discipline;
    private List<V> constituent;

    public Sport(K discipline, List<V> constituent) {
        this.discipline = discipline;
        this.constituent = constituent;
    }

    public void cantLiveWithout() {
        System.out.println("<" + discipline.description + "> It can't live without <" + constituent + ">");
    }
}
